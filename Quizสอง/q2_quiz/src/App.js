import logo from './logo.svg';
import './App.css';
import {Route, Routes } from "react-router-dom";
import About from './component/About';
import Header from './component/Header';
import OddEven from './component/OddEven';
import OddEvenpage from './page/OddEvenpage';
import Aboutpage from './page/Aboutpage';


function App() {
  return (
    <div className="App">
    <Header />
    <Routes> 
      <Route path="/" element={
              <OddEvenpage />
            } />
        <Route path="/about" element={
              <Aboutpage />
            } />
           
    </Routes>
  </div>
);
}

export default App;
