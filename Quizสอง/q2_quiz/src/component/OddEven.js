import OddEvenpage from "../page/OddEvenpage";

function OddEven (props) {

    return(

        <div>
            <h3>คุณ: {props.name}</h3>
            <h3> เลข: {props.number}</h3>
            <h3> {props.result} </h3>
        </div>
    );
}
export default OddEven;