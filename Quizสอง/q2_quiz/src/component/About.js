import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';


function About(props) {
    return(
        <Box sx={{width:"60%"}}>
            <Paper elevation={3}>
            <h2> จัดทำโดย: {props.name}</h2>
            <h3> รหัส: {props.pass}</h3>
            <h3> ศึกษาอยู่ที่: {props.lo} </h3>
            </Paper>
        </Box>
      
    );
}

export default About;