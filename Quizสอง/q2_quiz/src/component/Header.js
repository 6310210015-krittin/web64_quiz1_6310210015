import {Link} from "react-router-dom";
import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';

function Header() {
    return (
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6">
                ยินดีต้อนรับสู่เว็บเลขคู่ เลขคี่ หรือ 0
            </Typography> 
            &nbsp;&nbsp;      
            <Link to="/">
                <Typography variant="body1">
                    คำนวณเลขคู่ เลขคี่ หรือ 0 
                    </Typography>
            </Link>
          &nbsp;&nbsp;&nbsp; 
            <Link to="/about">
                <Typography variant="body1">
                    ผู้จัดทำ
                    </Typography>
            </Link>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
export default Header;