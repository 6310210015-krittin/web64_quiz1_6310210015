import { Grid, Typography, Box, Container } from "@mui/material";
import { useState } from "react";
import Button from '@mui/material/Button';
import OddEven from "../component/OddEven";

function OddEvenpage (props){

    const [Name, setName ] = useState("");
    const [Number, setNumber ] = useState("");
    const [Result, setResult ] = useState("");

    function evenOdd() {
        let num = parseInt(Number);
        setNumber(num);
        if(  num   == 0 ) {
            setResult("เป็น 0")
        } else if((num % 2) == 0) {
            setResult("เป็นเลขคู่")
        } else {
            setResult("เป็นเลขคี่")
        }
    }

    return(
        <Container maxwidth='lg'>
        <Grid container spacing={2} sx = { {marginTop : "10px" }}>
            <Grid item xs={12}>
            <Typography variant="h5">ยินดีต้อนรับสู่เว็บคำนวณ เลขคู่ เลขคี่ หรือ 0</Typography>
            </Grid>
      <Grid item xs={8} >
        <Box sx={{ textAlign : 'left'}} >   
                คุณชื่อ: <input type="text"
                    value={Name}
                    onChange={(e) => {setName(e.target.value);} } /> 
                    <br />
                    <br />
                ป้อนตัวเลข: <input type="text" 
                    value = {Number}
                    onChange={(e) => {setNumber(e.target.value);} }/><br/>
                    <br />
                    <Button variant="contained" onClick={() => evenOdd()}>
                        Calculate
                    </Button>
        </Box>
      </Grid>
      <Grid item xs={4}>
        {
            <div>
                <hr />
             นี่คือผลการคำนวณ:

            <OddEven 
            name = {Name}
            number = {Number}
            result = {Result}
            />
            </div>
            }
             
  </Grid>
    </Grid>
    </Container>
    );
}    

export default OddEvenpage;     